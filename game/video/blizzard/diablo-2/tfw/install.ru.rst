Установка гроздьев гнева (The Fury Within)
==========================================

:Сработало: март 2019
:ОС: виндоуз 10, убунту 18.04
:Метки: diablo 2, grapes of wrath, patch 1.09, english version

- 1. Качаешь `английскую версию с ТГВ <https://d2craft.ru/download-diablo-2-the-grapes-of-wrath-the-english-version.html>`_.

  - 1.1 Внимание, пароль архива: ``delasierra``.
  - 1.2 Распаковываешь в удобное место.

- 2. Качаешь `новейший плагИ 11.02 <http://plugy.free.fr/PlugY_The_Survival_Kit_v11.02.zip>`_.

  - 2.1 Распаковываешь в папку игры.
  - 2.2 :download:`Прикрепленный PlugY.ini <cfg/PlugY.ini>` копируешь с заменой в папку игры.

- 3. Запускаешь :file:`d2VidTst.exe`, выбираешь д3д.
- 4. Запускаешь :file:`PlugY.exe`.

Если не сработало
-----------------

- 4.1.1 Создаешь бэкап :file:`d2gfx.dll`,
- 4.1.2 запускаешь :file:`patchD2file.exe`,
- 4.1.3 запускаешь :file:`d2loader.exe`.

В завершение
------------

.. important::
    Обязательно проверь, что на главном экране справа от кнопок есть надпись PlugY 11.02.
