Редактор
========

Подойдет любой блокнот, упрощающий работу с реСтруктурированнымТекстом (reStructuredText).

Для старта мы предлагаем `Visual Studio Code <https://code.visualstudio.com/>`_.
Из плюсов:

#. Он есть под все платформы.
#. Довольно хорошо локализован.
#. Поддерживает контроль версий.
#. Умеет предварительный просмотр реСТ.

`Единственный найденный WYSIWYG <https://github.com/bigov/everest>`_
в Убунту 18.04 не собрался.

Вот что еще нашлось:

- http://rst.ninjs.org/ - онлайн вариант;
- https://github.com/retext-project/retext - блокнот-приложение на QT и Пайтоне;
- https://github.com/ShiraNai7/rstpad - еще одно на QT и C++;
- и даже `плагин для gedit <https://github.com/bittner/gedit-reST-plugin>`_.

В пределе хотелось бы нечто вроде "`Типоры <https://typora.io/>`_",
но для реСТа.

И совсем странные находки приведем для полноты:

- `NoTex <https://github.com/hsk81/notex-v2.0>`_ заявляет, что умеет реСТ.
  В `онлайн-версии <https://www.notex.ch/editor>`_ поддержка не найдена.
- https://github.com/limodou/ulipad - еще один блокнот с предпросмотром.
- https://github.com/ondratu/formiko - и еще один, оба на Пайтоне.