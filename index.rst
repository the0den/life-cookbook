.. The life cookbook documentation master file, created by
   sphinx-quickstart on Mon Mar 25 01:26:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The life cookbook's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lcb/editor.ru
   game/video/blizzard/diablo-2/tfw/install.ru


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
